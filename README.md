# Compiler Programing Language

@petrosyan.2001/compiler is a Node.js library for dealing with compiler programing language.
OS only Linux (Ubuntu)

## Installation

Use the package manager [npm](https://www.npmjs.com/) to install @petrosyan.2001/compiler.

```bash
npm install @petrosyan.2001/compiler
```

Or [yarn](https://yarnpkg.com/)

```bash
yarn add @petrosyan.2001/compiler
```

## Usage

```js
import { Compiler } from "@petrosyan.2001/compiler"

const compiler = new Compiler();

compiler.start();
```

## License
[MIT](https://choosealicense.com/licenses/mit/)
